<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Foto;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionGetListaIniciativasProductivas(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $iniciativas_productivas = Yii::$app->db->createCommand("
                        select
                            x.formulario_iniciativa,
                            x.tipo_formulario,
                            x.id_id_informacion_general,
                            x.id_caracteristica_general,
                            x.nombre_departamento,
                            x.nombre_provincia,
                            x.nombre_distrito,
                            x.id_plataforma,
                            x.nombre_tambo,
                            x.nombre_iniciativa_productiva,
                            x.consiste_iniciativa_productiva,
                            x.cant_fm_bn_iniciativa_productiva,
                            x.empleado_nombre,
                            x.empleado_correo,
                            x.puesto,
                            x.id_caracteristica_especifica_ptfma_promovida
                        from 
                        (
                            select 
                            'INICIATIVAS TAMBO' as formulario_iniciativa,
                            1 as tipo_formulario,
                            ip.id_informacion_general_ptfma_promovida as id_id_informacion_general,
                            ct.id_caracteristica_general_ptfma_promovida as id_caracteristica_general,
                            cp.nombre_departamento,
                            cp.nombre_provincia,
                            cp.nombre_distrito,
                            ip.id_plataforma,
                            cp.nombre_tambo,
                            et.id_caracteristica_especifica_ptfma_promovida,
                            et.nombre_iniciativa_productiva,
                            et.consiste_iniciativa_productiva,
                            '' AS cant_fm_bn_iniciativa_productiva,
                            (
                                 select 
                                  concat(per.apellidos_persona,' ',nombres_persona) as gestor
                                 from TambosDb.dbo.tb_acceso_tambo act
                                 inner  join TambosDb.dbo.tb_usuarios_accesos uac on (uac.id_acceso = act.id_acceso and uac.estado = 1
                                  and uac.estado_acceso = 1 and uac.id_area_cargo_laboral = 8)
                                 inner join TambosDb.dbo.tb_usuarios usu on (uac.id_usuario = usu.id_usuario and usu.estado= 1 and usu.estado_usuario = 1)
                                 inner join TambosDb.dbo.tb_general_personas per on (usu.id_persona = per.id_persona and per.estado=1
                                 )
                                 where
                                  act.id_tambo = tax.id_tambo
                                  and act.estado = 1
                                  and act.estado_acceso = 1 
                              ) as empleado_nombre,
                                (
                                 select 
                                 usu.correo_usuario
                                 from TambosDb.dbo.tb_acceso_tambo act
                                 inner  join TambosDb.dbo.tb_usuarios_accesos uac on (uac.id_acceso = act.id_acceso and uac.estado = 1
                                  and uac.estado_acceso = 1 and uac.id_area_cargo_laboral = 8)
                                 inner join TambosDb.dbo.tb_usuarios usu on (uac.id_usuario = usu.id_usuario and usu.estado= 1 and usu.estado_usuario = 1)
                                 inner join TambosDb.dbo.tb_general_personas per on (usu.id_persona = per.id_persona and per.estado=1
                                 )
                                 where
                                  act.id_tambo = tax.id_tambo
                                  and act.estado = 1
                                  and act.estado_acceso = 1 
                              ) as empleado_correo,
                                (
                                 select 
                             cl.nombre_cargo_laboral
                                 from TambosDb.dbo.tb_acceso_tambo act
                                 inner  join TambosDb.dbo.tb_usuarios_accesos uac on (uac.id_acceso = act.id_acceso and uac.estado = 1
                                  and uac.estado_acceso = 1 and uac.id_area_cargo_laboral = 8)
                                 inner join TambosDb.dbo.tb_usuarios usu on (uac.id_usuario = usu.id_usuario and usu.estado= 1 and usu.estado_usuario = 1)
                                 inner join TambosDb.dbo.tb_general_personas per on (usu.id_persona = per.id_persona and per.estado=1)
                                inner join TambosDb.dbo.tb_general_cargos_laborales cl on (uac.id_area_cargo_laboral = cl.id_cargo_laboral)
                                 where
                                  act.id_tambo = tax.id_tambo
                                  and act.estado = 1
                                  and act.estado_acceso = 1 
                              ) as puesto
                            from PAISDB.catalogacion_iniciativa_productiva.informacion_general_ptfma_promovida ip
                            inner join PAISDB.maestras.ccpps_actualizados cp on (cp.id_plataforma = ip.id_plataforma and cp.clasificacion_pais ='TAMBO')
                            inner join PAISDB.catalogacion_iniciativa_productiva.caracteristica_general_ptfma_promovida ct on (ip.id_informacion_general_ptfma_promovida = ct.id_informacion_general_ptfma_promovida and ip.flg_finalizado = 1 and ct.fecha_del is null )
                            inner join PAISDB.catalogacion_iniciativa_productiva.caracteristica_especifica_ptfma_promovida et on (ip.id_informacion_general_ptfma_promovida = et.id_informacion_general_ptfma_promovida 
                             and et.fecha_del is null)
                            inner join TambosDb.dbo.tb_general_tambos tax on (cp.id_plataforma = tax.id_tambo)
                            inner join TambosDb.dbo.tb_general_tambos_temporal tex on (tax.id_tambo = tex.id_tam and tex.estado = 1 and tex.servicio = 1) 
                        )x
                        
                        order by  x.nombre_departamento asc
                    ")->queryAll();
            return ['success'=>true,'iniciativas_productivas'=>$iniciativas_productivas];
        }
        
    }

    public function actionTablero($tipo_formulario=null,$id_id_informacion_general=null,$id_caracteristica_general=null,$id_caracteristica_especifica_ptfma_promovida=null){
        $this->layout="vacio";
        if($tipo_formulario && $id_id_informacion_general){
            $iniciativa_productiva = Yii::$app->db->createCommand("
                        select
                            x.formulario_iniciativa,
                            x.tipo_formulario,
                            x.id_id_informacion_general,
                            x.id_caracteristica_general,
                            x.nombre_departamento,
                            x.nombre_provincia,
                            x.nombre_distrito,
                            x.id_plataforma,
                            x.nombre_tambo,
                            x.nombre_iniciativa_productiva,
                            x.consiste_iniciativa_productiva,
                            x.cant_fm_bn_iniciativa_productiva,
                            x.empleado_nombre,
                            x.empleado_correo,
                            x.puesto,
                            x.id_caracteristica_especifica_ptfma_promovida
                        from 
                        (
                            select 
                            'INICIATIVAS TAMBO' as formulario_iniciativa,
                            1 as tipo_formulario,
                            ip.id_informacion_general_ptfma_promovida as id_id_informacion_general,
                            ct.id_caracteristica_general_ptfma_promovida as id_caracteristica_general,
                            cp.nombre_departamento,
                            cp.nombre_provincia,
                            cp.nombre_distrito,
                            ip.id_plataforma,
                            cp.nombre_tambo,
                            et.id_caracteristica_especifica_ptfma_promovida,
                            et.nombre_iniciativa_productiva,
                            et.consiste_iniciativa_productiva,
                            
                            '' AS cant_fm_bn_iniciativa_productiva,
                            (
                                 select 
                                  concat(per.apellidos_persona,' ',nombres_persona) as gestor
                                 from TambosDb.dbo.tb_acceso_tambo act
                                 inner  join TambosDb.dbo.tb_usuarios_accesos uac on (uac.id_acceso = act.id_acceso and uac.estado = 1
                                  and uac.estado_acceso = 1 and uac.id_area_cargo_laboral = 8)
                                 inner join TambosDb.dbo.tb_usuarios usu on (uac.id_usuario = usu.id_usuario and usu.estado= 1 and usu.estado_usuario = 1)
                                 inner join TambosDb.dbo.tb_general_personas per on (usu.id_persona = per.id_persona and per.estado=1
                                 )
                                 where
                                  act.id_tambo = tax.id_tambo
                                  and act.estado = 1
                                  and act.estado_acceso = 1 
                              ) as empleado_nombre,
                                (
                                 select 
                                 usu.correo_usuario
                                 from TambosDb.dbo.tb_acceso_tambo act
                                 inner  join TambosDb.dbo.tb_usuarios_accesos uac on (uac.id_acceso = act.id_acceso and uac.estado = 1
                                  and uac.estado_acceso = 1 and uac.id_area_cargo_laboral = 8)
                                 inner join TambosDb.dbo.tb_usuarios usu on (uac.id_usuario = usu.id_usuario and usu.estado= 1 and usu.estado_usuario = 1)
                                 inner join TambosDb.dbo.tb_general_personas per on (usu.id_persona = per.id_persona and per.estado=1
                                 )
                                 where
                                  act.id_tambo = tax.id_tambo
                                  and act.estado = 1
                                  and act.estado_acceso = 1 
                              ) as empleado_correo,
                                (
                                 select 
                             cl.nombre_cargo_laboral
                                 from TambosDb.dbo.tb_acceso_tambo act
                                 inner  join TambosDb.dbo.tb_usuarios_accesos uac on (uac.id_acceso = act.id_acceso and uac.estado = 1
                                  and uac.estado_acceso = 1 and uac.id_area_cargo_laboral = 8)
                                 inner join TambosDb.dbo.tb_usuarios usu on (uac.id_usuario = usu.id_usuario and usu.estado= 1 and usu.estado_usuario = 1)
                                 inner join TambosDb.dbo.tb_general_personas per on (usu.id_persona = per.id_persona and per.estado=1)
                                inner join TambosDb.dbo.tb_general_cargos_laborales cl on (uac.id_area_cargo_laboral = cl.id_cargo_laboral)
                                 where
                                  act.id_tambo = tax.id_tambo
                                  and act.estado = 1
                                  and act.estado_acceso = 1 
                              ) as puesto
                            from PAISDB.catalogacion_iniciativa_productiva.informacion_general_ptfma_promovida ip
                            inner join PAISDB.maestras.ccpps_actualizados cp on (cp.id_plataforma = ip.id_plataforma and cp.clasificacion_pais ='TAMBO')
                            inner join PAISDB.catalogacion_iniciativa_productiva.caracteristica_general_ptfma_promovida ct on (ip.id_informacion_general_ptfma_promovida = ct.id_informacion_general_ptfma_promovida and ip.flg_finalizado = 1 and ct.fecha_del is null )
                            inner join PAISDB.catalogacion_iniciativa_productiva.caracteristica_especifica_ptfma_promovida et on (ip.id_informacion_general_ptfma_promovida = et.id_informacion_general_ptfma_promovida 
                             and et.fecha_del is null)
                            inner join TambosDb.dbo.tb_general_tambos tax on (cp.id_plataforma = tax.id_tambo)
                            inner join TambosDb.dbo.tb_general_tambos_temporal tex on (tax.id_tambo = tex.id_tam and tex.estado = 1 and tex.servicio = 1) 
                        ) x
                        where x.tipo_formulario = '" . $tipo_formulario . "' and x.id_id_informacion_general = ".$id_id_informacion_general." and x.id_caracteristica_general = ".$id_caracteristica_general." and x.id_caracteristica_especifica_ptfma_promovida = ".$id_caracteristica_especifica_ptfma_promovida."
                        order by  x.nombre_departamento asc
                    ")->queryOne();
                
                    $imagenesLista = Yii::$app->db->createCommand("select top 4 a.* from PAISDB.catalogacion_iniciativa_productiva.archivos a
                    where a.id_especifica_tambo=".$id_caracteristica_especifica_ptfma_promovida." and a.flg_tipo=2 and a.fecha_del is null;")->queryAll();
                    
                    $imagen1 = ($imagenesLista && isset($imagenesLista[0]))?"https://www.pais.gob.pe/backendsismonitor/public/storage/".$imagenesLista[0]['path']:"";
                    $imagen2 = ($imagenesLista && isset($imagenesLista[1]))?"https://www.pais.gob.pe/backendsismonitor/public/storage/".$imagenesLista[1]['path']:"";
                    $imagen3 = ($imagenesLista && isset($imagenesLista[2]))?"https://www.pais.gob.pe/backendsismonitor/public/storage/".$imagenesLista[2]['path']:"";
                    $imagen4 = ($imagenesLista && isset($imagenesLista[3]))?"https://www.pais.gob.pe/backendsismonitor/public/storage/".$imagenesLista[3]['path']:"";
                
            return $this->render('tablero',['iniciativa_productiva'=>$iniciativa_productiva,'imagen1'=>$imagen1,'imagen2'=>$imagen2,'imagen3'=>$imagen3,'imagen4'=>$imagen4]);
        }

        return 'no permitido';
        
    }

    public function actionGetRegiones(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $regiones = Yii::$app->db->createCommand("
                        select distinct nombre_departamento from PAISDB.maestras.ccpps_actualizados order by nombre_departamento asc;
                    ")->queryAll();
            return [
                'success'=>true,
                'regiones'=>$regiones
            ];
        }
    }


    public function actionGetTambos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $nombre_departamento = $_POST['region'];
            $tambos = Yii::$app->db->createCommand("
                        select distinct nombre_tambo from PAISDB.maestras.ccpps_actualizados where nombre_departamento='".$nombre_departamento."' order by nombre_tambo asc;
                    ")->queryAll();
            return [
                'success'=>true,
                'tambos'=>$tambos
            ];
        }
    }

    public function actionImagenes($id_caracteristica_especifica_ptfma_promovida=null,$id_caracteristica_general=null,$id_id_informacion_general=null,$tipo_formulario=null){
        return $this->render('imagenes',['id_caracteristica_especifica_ptfma_promovida'=>$id_caracteristica_especifica_ptfma_promovida]);
    }

    public function actionGetListaImagenesIniciativaProductiva(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_caracteristica_especifica_ptfma_promovida = $_POST['id_caracteristica_especifica_ptfma_promovida'];
            $imagenes = (new \yii\db\Query())->select('*')->from('PAISDB.catalogacion_iniciativa_productiva.archivos')
                        ->where('id_especifica_tambo=:id_especifica_tambo and fecha_del is null',[':id_especifica_tambo'=>$id_caracteristica_especifica_ptfma_promovida])
                        ->all();

            return [
                'success'=>true,
                'imagenes'=>$imagenes
            ];
        }
    }

    public function actionDescargar($path=null){
        if(isset($_GET['path']))
        {
           
            //Read the filename
            $urlFile = "https://www.pais.gob.pe/backendsismonitor/public/storage/".$_GET['path'];


            $file_name  =   basename($urlFile);
            //save the file by using base name
            $fn         =   file_put_contents($file_name,file_get_contents($urlFile));
            header("Expires: 0");
            header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header("Content-type: application/file");
            header('Content-length: '.filesize($file_name));
            header('Content-disposition: attachment; filename="'.basename($file_name).'"');
            readfile($file_name);
            
        }
    }


    public function actionActualizarFoto($id_archivo){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Foto::findOne($id_archivo);
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->archivo = UploadedFile::getInstance($model, 'archivo');
                if($model->archivo){
                    if($model->path){
                        //var_dump(Yii::$app->basePath . '/web/storage/'.$model->path);die;
                        if (file_exists(Yii::$app->basePath . '/web/storage/'.$model->path)) {
                            unlink(Yii::$app->basePath . '/web/storage/'.$model->path);
                        } 
                    }
                    $tempName = Yii::$app->security->generateRandomString(40);
                    //var_dump(Yii::$app->security->generateRandomString(40) );die;
                    if($model->archivo->saveAs('storage/convenios/archivos/' . $tempName . '.' . $model->archivo->extension)){
                        $model->path = 'convenios/archivos/'. $tempName  . '.' . $model->archivo->extension;
                        if($model->save()){
                            return ['success'=>true];
                        }else{
                            return ['success'=>false];
                        }
                    }
                }
                return ['success'=>false];
            }
        }
    }
}
