<?php

namespace app\models;

use Yii;

class Foto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $archivo;
    public $temp;
    public static function tableName()
    {
        return 'PAISDB.catalogacion_iniciativa_productiva.archivos';
    }
    

    public static function primaryKey()
    {
        return ["id_archivo"];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           
            [['archivo','temp'], 'safe'],
        ];
    }

}
