<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>Iniciativas Productivas | MIDIS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->


    <!-- Bootstrap Css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/app.min.css" rel="stylesheet" type="text/css" />
    <!-- Custom Css-->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/custom.css" rel="stylesheet" type="text/css" />
    

    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Lightbox css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />

    <!-- JAVASCRIPT -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jquery/jquery.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/metismenu/metisMenu.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/simplebar/simplebar.min.js"></script>

    <!-- Required datatable js -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    
    <!-- Responsive examples -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/js/jquery.expander.js"></script>
    
    <!-- Magnific Popup-->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/magnific-popup/jquery.magnific-popup.min.js"></script>

    <style>
    body[data-topbar=dark] #page-topbar{
        background-color:#a83712;
    }
    .navbar-header{
        background-color:#a83712;
    }


    body[data-layout=horizontal] .page-content {
        padding:calc(80px + 24px) calc(24px / 2) 60px calc(24px / 2);
    }

    body[data-topbar=dark] .logo-light{
        padding-top:32px;
    }
    

    .navbar-header{
        height: 76px;
        padding-left: 15px;
        padding-right: 15px;
        margin-bottom: 0;
        border-bottom: 1px solid;
        border-color: #a83712;
        min-width: 230px !important;
    }
   
   .seccion_1{
        font-size:20px;
        border-top:10px solid #00AEC4;
        border-bottom:1px solid #00AEC4;
        margin-top:-17px;
        color:#00AEC4;
    }
    .seccion_2{
        padding-top:10px;
    }
    .seccion_2 .seccion_2_contenido_1{
        padding:10px;
    }

    .seccion_2 .seccion_2_contenido_1 img{
        width:300px;
        height:250px;
    }
    .seccion_2 .seccion_2_contenido_2 .titulo_1{
        color:#737374;
        font-size:18px;
    }
    .seccion_2 .seccion_2_contenido_2 .titulo_2{
        color:#BE1819;
        font-size:25px;
        text-align: justify;
    }

    .seccion_2 .seccion_2_contenido_2 .descripcion_principales{
        font-size:16px;
        text-align: justify;
    }

    .seccion_2 .seccion_2_contenido_2 .seccion_2_contenido_2_descripcion_2{
        border-top:1px solid #387FB4;
        border-bottom:1px solid #387FB4;
    }
    .seccion_2 .seccion_2_contenido_2 .seccion_2_contenido_2_descripcion_2 .descripcion{
        color:#387FB4;
        margin-top:15px;
        font-size:20px
    }
    .seccion_3{
        padding-top:10px;
    }

    .seccion_3 .seccion_3_contenido_1{
        background-color:#E8E8E8;
    }

    .seccion_3 .seccion_3_contenido_1 .seccion_3_contenido_1_datos{
        padding-left:10px;
        padding-right:10px;
    }

    .seccion_3 .seccion_3_contenido_1 .seccion_3_contenido_1_datos .resena{
        text-align: justify;
    }


    .seccion_3 .seccion_3_contenido_1 .seccion_3_contenido_1_datos .media{
        padding-top:10px;
        padding-bottom:10px;
    }

    .seccion_3 .seccion_3_contenido_1 .icono{
        font-size:36px
    }

    .seccion_3 .seccion_3_contenido_2{
        /* padding-top:10px; */
    }

    .seccion_3 .seccion_3_contenido_2 img{
        width:160px;
        height:200px;
    }

    

    @media (max-width: 1920px){
        .seccion_2 .seccion_2_contenido_1 img{
            width:100%;
            height:350px;
        }

        .seccion_2 .seccion_2_contenido_2 .titulo_1{
            font-size:18px;
        }
        .seccion_2 .seccion_2_contenido_2 .titulo_2{
            font-size:16px;
        }

        .seccion_2 .seccion_2_contenido_2 .descripcion_principales{
            font-size:14px;
        }

        .seccion_2 .seccion_2_contenido_2 .seccion_2_contenido_2_descripcion_2 .descripcion{
            font-size: 16px;
        }
    }

    @media (max-width: 1600px){
        .seccion_2 .seccion_2_contenido_1 img{
            width:100%;
            height:350px;
        }

        .seccion_2 .seccion_2_contenido_2 .titulo_1{
            font-size:18px;
        }
        .seccion_2 .seccion_2_contenido_2 .titulo_2{
            font-size:16px;
        }

        .seccion_2 .seccion_2_contenido_2 .descripcion_principales{
            font-size:14px;
        }
    }

    @media (max-width: 1480px){
        .seccion_2 .seccion_2_contenido_1 img{
            width:100%;
            height:350px;
        }

        .seccion_2 .seccion_2_contenido_2 .titulo_1{
            font-size:18px;
        }
        .seccion_2 .seccion_2_contenido_2 .titulo_2{
            font-size:16px;
        }

        .seccion_2 .seccion_2_contenido_2 .descripcion_principales{
            font-size:14px;
        }
    }

    @media (max-width: 1280px){
        .seccion_2 .seccion_2_contenido_1 img{
            width:100%;
            height:400px;
        }

        .seccion_2 .seccion_2_contenido_2 .titulo_1{
            font-size:18px;
        }
        .seccion_2 .seccion_2_contenido_2 .titulo_2{
            font-size:16px;
        }

        .seccion_2 .seccion_2_contenido_2 .descripcion_principales{
            font-size:14px;
        }
    }

    @media (max-width: 1024px){
        .seccion_2 .seccion_2_contenido_1 img{
            width:100%;
            height:310px;
        }

        .seccion_2 .seccion_2_contenido_2 .titulo_1{
            font-size:18px;
        }
        .seccion_2 .seccion_2_contenido_2 .titulo_2{
            font-size:16px;
        }

        .seccion_2 .seccion_2_contenido_2 .descripcion_principales{
            font-size:11px;
        }
        .seccion_2 .seccion_2_contenido_2 .seccion_2_contenido_2_descripcion_2 .descripcion{
            font-size:13px;
        }
        

        .seccion_3 .seccion_3_contenido_2 img{
            width:100%;
            height:200px;
        }
    }
    
    @media (max-width: 965px){
        .modal-dialog{
            max-width:800px;
        }

        .seccion_1{
            font-size:17px;
        }
        .seccion_2 .seccion_2_contenido_1 img{
            width:100%;
            height:250px;
        }

        .seccion_2 .seccion_2_contenido_2 .titulo_1{
            font-size:18px;
        }
        .seccion_2 .seccion_2_contenido_2 .titulo_2{
            font-size:14px;
        }

        .seccion_2 .seccion_2_contenido_2 .descripcion_principales{
            font-size:14px;
        }

        .seccion_3 .seccion_3_contenido_2 img{
            width:100%;
            height:200px;
        }

    }

    @media (max-width: 725px){
        .modal-dialog{
            max-width:500px;
        }

        .seccion_1{
            font-size:17px;
        }
        .seccion_2 .seccion_2_contenido_1 img{
            width:100%;
            height:250px;
        }

        .seccion_2 .seccion_2_contenido_2 .titulo_1{
            font-size:18px;
        }
        .seccion_2 .seccion_2_contenido_2 .titulo_2{
            font-size:16px;
        }

        .seccion_2 .seccion_2_contenido_2 .descripcion_principales{
            font-size:14px;
        }

        .seccion_2 .seccion_2_contenido_2 .seccion_2_contenido_2_descripcion_2 .descripcion{
            font-size:16px;
        }
        
        .seccion_3 .seccion_3_contenido_2 .col-12{
            margin-top:5px;
            margin-bottom:5px;
            padding:0px;
        }

        .seccion_3 .seccion_3_contenido_2 img{
            width:100%;
            height:200px;
        }
    }

    @media (max-width: 400px){
        .modal-dialog{
            max-width:500px;
        }

        .seccion_1{
            font-size:13px;
        }
        .seccion_2 .seccion_2_contenido_1 img{
            width:100%;
            height:250px;
        }
        .seccion_2 .seccion_2_contenido_2 .titulo_1{
            font-size:18px;
        }
        .seccion_2 .seccion_2_contenido_2 .titulo_2{
            font-size:16px;
        }
        .seccion_2 .seccion_2_contenido_2 .descripcion_principales{
            font-size:14px;
        }

        .seccion_2 .seccion_2_contenido_2 .seccion_2_contenido_2_descripcion_2 .descripcion{
            font-size:16px;
        }

        .seccion_3 .seccion_3_contenido_2 .col-12{
            margin-top:5px;
            margin-bottom:5px;
            padding:0px;
        }

        .seccion_3 .seccion_3_contenido_2 img{
            width:100%;
            height:200px;
        }
    }
   </style>
    
</head>

<body data-topbar="dark" data-layout="horizontal">
    <!-- <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog text-center" role="document">
            Cargando <br>
            <div class="spinner-border text-primary m-1" role="status"></div>
        </div>
    </div> -->
    <!-- Begin page -->

    
    <div id="layout-wrapper">
        <header id="page-topbar">
            <div class="navbar-header">
                <div class="d-flex">
                    <!-- LOGO -->
                    <div class="navbar-brand-box">
                        <a href="index.html" class="logo logo-dark">
                            <span class="logo-sm">
                                <img src="https://www.pais.gob.pe/tambos/themes/amachay/assets/img/logomin2.png" alt="" height="100">
                            </span>
                            <span class="logo-lg">
                                <img src="https://www.pais.gob.pe/tambos/themes/amachay/assets/img/logomin2.png" alt="" height="100">
                            </span>
                        </a>

                        <a href="index.html" class="logo logo-light">
                            <span class="logo-sm">
                                <img src="https://www.pais.gob.pe/tambos/themes/amachay/assets/img/logomin2.png" alt="" height="100">
                            </span>
                            <span class="logo-lg">
                                <img src="https://www.pais.gob.pe/tambos/themes/amachay/assets/img/logomin2.png" alt="" height="100">
                            </span>
                        </a>
                    </div>

                </div>

            </div>
        </header> <!-- ========== Left Sidebar Start ========== -->
        <div class="main-content" id="result">
            <div class="page-content" style="margin-top:0px">
                <div class="container-fluid">
                    <?= $content ?>
                </div>
            </div>
        </div>

    </div>
    <!-- END layout-wrapper -->

    
    <!-- /Right-bar -->
    <!-- Right bar overlay-->
    
    
</body>

</html>