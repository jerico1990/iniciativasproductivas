
<div class="modal-body">
    <div class="row seccion_1">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-left">
            REGIÓN. <b> <?= $iniciativa_productiva['nombre_departamento'] ?> </b>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-right">
            CATALOGO PRODUCTIVO
        </div>
    </div>
    
    <div class="row seccion_2">
        <div class="col-md-6 seccion_2_contenido_1" >
            <div class="tab-pane fade show active" id="product-1" role="tabpanel" aria-labelledby="product-1-tab">
                <div>
                    <img  src="<?= $imagen1 ?>" class="img-fluid mx-auto d-block">
                </div>
            </div>
        </div>
        <div class="col-md-6 seccion_2_contenido_2">
            <div class="row">
                <div class="col-md-12 seccion_2_contenido_2_descripcion_1">
                    <p class="titulo_1"> <b> INICIATIVA PRODUCTIVA </b></p>
                    <p class="titulo_2"> <b> <?= $iniciativa_productiva['nombre_iniciativa_productiva'] ?> </b></p>
                    <p class="descripcion_principales">Describa brevemente las principales características de la iniciativa productiva. <?= $iniciativa_productiva['consiste_iniciativa_productiva'] ?></p>
                </div>
                <div class="col-md-12 seccion_2_contenido_2_descripcion_2" >
                    <p class="descripcion"><?= $iniciativa_productiva['cant_fm_bn_iniciativa_productiva'] ?> familias del distrito de <?= $iniciativa_productiva['nombre_distrito'] ?> en la provincia de <?= $iniciativa_productiva['nombre_provincia'] ?> se están beneficiando de esta compra.</p>
                </div>
            </div>
            
        </div>
    </div>

    <div class="row seccion_3">
        <div class="col-md-6 seccion_3_contenido_1" >
            <div class="row seccion_3_contenido_1_datos" >
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                    <div class="media" >
                        <div class="avatar-xs mr-3">
                            <span class="fas fa-user-tie icono"></span>
                        </div>
                        <div class="media-body">
                            <h5> DATOS DE CONTACTO </h5>
                            <span></span> <small> <?= $iniciativa_productiva['empleado_nombre'] ?>  </small> <br>
                            <span class="fas fa-at "></span> <small> <?= $iniciativa_productiva['empleado_correo'] ?> </small> <br>
                            <span class="fas fa-phone "></span> <small>  </small>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                    <div class="media" >
                        <div class="avatar-xs mr-3">
                            <span class="fas fa-clipboard-list icono "></span>
                        </div>
                        <div class="media-body">
                            <h5> RESEÑA DEL PRODUCTOR </h5>
                            <small class="resena"> <?= $iniciativa_productiva['consiste_iniciativa_productiva'] ?> </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 seccion_3_contenido_2">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <?php if($imagen2){ ?>
                        <img src="<?= $imagen2 ?>"  class="Responsive image avatar-xxl">
                    <?php } ?>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <?php if($imagen3){ ?>
                        <img src="<?= $imagen3 ?>"  class="Responsive image avatar-xxl">
                    <?php } ?>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <?php if($imagen4){ ?>
                        <img src="<?= $imagen4 ?>"  class="Responsive image avatar-xxl">
                    <?php } ?>
                </div>
            </div>
            
        </div>
    </div>

</div>
