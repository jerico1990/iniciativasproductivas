
<style>
.dataTables_filter{
    display:none;
}
</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">REGIÓN</label>
                            <select id="region" class="form-control">
                                <option value>SELECCIONAR REGIÓN</option>
                            </select>
                            <!-- <input type="text" class="form-control" id="region" placeholder="Búsqueda por Región"> -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">TAMBO</label>
                            <select id="tambo" class="form-control">
                                <option value>SELECCIONAR TAMBO</option>
                            </select>
<!-- 
                            <input type="text" class="form-control" id="tambo" placeholder="Búsqueda por Tambo"> -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">INICIATIVA PRODUCTIVA</label>
                            <input type="text" class="form-control" id="iniciativa-productiva" placeholder="Búsqueda por Iniciativa Productiva">
                        </div>
                    </div>

                    
                </div>

                <div class="row">
                
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary btn-limpiar float-start">LIMPIAR</button>
                            <button type="button" class="btn btn-primary btn-buscar-iniciativa-productiva float-end">BUSCAR</button>
                        </div>
                    </div>
                </div>

                <table id="lista-iniciativas-productivas" class="table table-bordered dt-responsive  lista-iniciativas-productivas"
                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            
                            <th>Región</th>
                            <th>Provincia</th>
                            <th>Distrito</th>
                            <th>Tambo</th>
                            <th>Iniciativa Productiva</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>


                    <tbody>
                        
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content" style="border-radius:0px">
        </div>
    </div>
</div>

<div class="modal" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog text-center" role="document">
        <i class="fa fa-refresh fa-spin"></i>
        Cargando
    </div>
</div>



<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');

IniciativasProductivas();
async function IniciativasProductivas(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/site/get-lista-iniciativas-productivas',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var iniciativas_productivas ="";
                        $('.lista-iniciativas-productivas').DataTable().destroy();
                        $.each(results.iniciativas_productivas, function( index, value ) {
                            
                            iniciativas_productivas = iniciativas_productivas + "<tr>";
                                
                                iniciativas_productivas = iniciativas_productivas + "<td> " + value.nombre_departamento + "</td>";
                                iniciativas_productivas = iniciativas_productivas + "<td>" + value.nombre_provincia + "</td>";
                                iniciativas_productivas = iniciativas_productivas + "<td>" + value.nombre_distrito + "</td>";
                                iniciativas_productivas = iniciativas_productivas + "<td>" + value.nombre_tambo + "</td>";
                                iniciativas_productivas = iniciativas_productivas + "<td class='nombre_iniciativa_productiva'>" + ((value.nombre_iniciativa_productiva)?value.nombre_iniciativa_productiva:"") + "</td>";

                                iniciativas_productivas = iniciativas_productivas + "<td>";
                                    iniciativas_productivas = iniciativas_productivas + '<a href="#" data-id_caracteristica_especifica_ptfma_promovida="' + value.id_caracteristica_especifica_ptfma_promovida + '" data-id_caracteristica_general="' + value.id_caracteristica_general + '" data-id_id_informacion_general="' + value.id_id_informacion_general + '" data-tipo_formulario="' + value.tipo_formulario + '" class="btn btn-primary btn-tablero" > <i class="fas fa-chalkboard-teacher"></i> </a> ';
                                    iniciativas_productivas = iniciativas_productivas + '<a href="<?= \Yii::$app->request->BaseUrl ?>/site/imagenes?id_caracteristica_especifica_ptfma_promovida='+ value.id_caracteristica_especifica_ptfma_promovida +'&id_caracteristica_general='+ value.id_caracteristica_general +'&id_id_informacion_general='+ value.id_id_informacion_general +'&tipo_formulario='+ value.tipo_formulario +'" class="btn btn-primary" > <i class="fas fas fa-image"></i> </a> ';
                                iniciativas_productivas = iniciativas_productivas +"</td>";
                            iniciativas_productivas = iniciativas_productivas + "</tr>";
                        });
                        
                        $('.lista-iniciativas-productivas tbody').html(iniciativas_productivas);

                        $('.nombre_iniciativa_productiva').expander({
                            slicePoint: 39, // si eliminamos por defecto es 100 caracteres
                            expandText: 'ver más', // por defecto es 'read more...'
                            collapseTimer: 0, // tiempo de para cerrar la expanción si desea poner 0 para no cerrar
                            userCollapseText: 'ver menos' // por defecto es 'read less...'
                        });

                        $('.lista-iniciativas-productivas').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){
                            loading.modal('hide');
                        },2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

$('body').on('click', '.btn-buscar-iniciativa-productiva', function (e) {
    $('.lista-iniciativas-productivas').DataTable().column(0).search($('#region').val()).draw();
    $('.lista-iniciativas-productivas').DataTable().column(3).search($('#tambo').val()).draw();
    $('.lista-iniciativas-productivas').DataTable().column(4).search($('#iniciativa-productiva').val()).draw();
});
$('body').on('click', '.btn-limpiar', function (e) {
    $('#region').val('');
    $('#tambo').val('');
    $('#iniciativa-productiva').val('');

    $('.lista-iniciativas-productivas').DataTable().column(0).search('').draw();
    $('.lista-iniciativas-productivas').DataTable().column(3).search('').draw();
    $('.lista-iniciativas-productivas').DataTable().column(4).search('').draw();
});



$('body').on('click', '.btn-tablero', function (e) {
    e.preventDefault();
    var tipo_formulario = $(this).attr('data-tipo_formulario');
    var id_id_informacion_general = $(this).attr('data-id_id_informacion_general');
    var id_caracteristica_general = $(this).attr('data-id_caracteristica_general');
    var id_caracteristica_especifica_ptfma_promovida = $(this).attr('data-id_caracteristica_especifica_ptfma_promovida');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/site/tablero?tipo_formulario='+ tipo_formulario + '&id_id_informacion_general='+id_id_informacion_general + '&id_caracteristica_general='+id_caracteristica_general+'&id_caracteristica_especifica_ptfma_promovida='+id_caracteristica_especifica_ptfma_promovida,function(){
        $('.descripcion_principales').expander({
            slicePoint: 130, // si eliminamos por defecto es 100 caracteres
            expandText: 'ver más', // por defecto es 'read more...'
            collapseTimer: 0, // tiempo de para cerrar la expanción si desea poner 0 para no cerrar
            userCollapseText: 'ver menos' // por defecto es 'read less...'
        });

        $('.resena').expander({
            slicePoint: 130, // si eliminamos por defecto es 100 caracteres
            expandText: 'ver más', // por defecto es 'read more...'
            collapseTimer: 0, // tiempo de para cerrar la expanción si desea poner 0 para no cerrar
            userCollapseText: 'ver menos' // por defecto es 'read less...'
        });
    });
    $('#modal').modal('show');
});

Regiones();
async function Regiones(){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/site/get-regiones",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var options_regiones = "<option value>SELECCIONAR REGIÓN</option>";
                        /* Seteando valores de detalle p01 */
                        $.each(results.regiones, function( index, value ) {
                            options_regiones = options_regiones + `<option value='${value.nombre_departamento}'>${value.nombre_departamento}</option>`;
                        });
                        $('#region').html(options_regiones);

                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

$('body').on('change', '#region', function (e) {
    region = $('#region').val();
    Tambos(region);
});


async function Tambos(region){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/site/get-tambos",
                method: "POST",
                data:{_csrf:csrf,region:region},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var options_tambos = "<option value>SELECCIONAR TAMBO</option>";
                        /* Seteando valores de detalle p01 */
                        $.each(results.tambos, function( index, value ) {
                            options_tambos = options_tambos + `<option value='${value.nombre_tambo}'>${value.nombre_tambo}</option>`;
                        });
                        $('#tambo').html(options_tambos);

                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

</script>