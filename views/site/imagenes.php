
<style>
.dataTables_filter{
    display:none;
}
</style>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                
                    <div class="col-md-12 ">
                        <div class="form-group ">
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/site" class="btn btn-primary pull-right"><i class="fas fa-arrow-left"></i> REGRESAR</a>
                        </div>
                    </div>
                </div>
                <table id="lista-imagenes" class="table table-bordered dt-responsive lista-imagenes"
                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            
                            <th>Imagen</th>
                            <th>Descarga</th>
                            <th>Reemplazar</th>
                        </tr>
                    </thead>


                    <tbody>
                        
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content" style="border-radius:0px">
        </div>
    </div>
</div>


<div class="modal" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog text-center" role="document">
        <i class="fa fa-refresh fa-spin"></i>
        Cargando
    </div>
</div>


<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var id_caracteristica_especifica_ptfma_promovida = "<?= $id_caracteristica_especifica_ptfma_promovida ?>";
Imagenes();
async function Imagenes(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/site/get-lista-imagenes-iniciativa-productiva',
                method: 'POST',
                data:{_csrf:csrf,id_caracteristica_especifica_ptfma_promovida:id_caracteristica_especifica_ptfma_promovida},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var imagen ="";
                        $('.lista-imagenes').DataTable().destroy();
                        $.each(results.imagenes, function( index, value ) {
                            
                            imagen = imagen + "<tr>";
                                imagen = imagen + "<td class='text-center'> ";
                                    imagen = imagen + "<a class='image-popup-vertical-fit' href='https://www.pais.gob.pe/backendsismonitor/public/storage/" + value.path + "' title='Imagen'>";
                                        imagen = imagen + "<img class='img-fluid' alt='' src='https://www.pais.gob.pe/backendsismonitor/public/storage/" + value.path + "' width='145'>";
                                    imagen = imagen + "</a>";
                                imagen = imagen + "</td>";
                                imagen = imagen + "<td> <a class='btn btn-success' target='_blank' href='<?= \Yii::$app->request->BaseUrl ?>/site/descargar?path=" + value.path + "' >Descargar</a> </td>";
                                imagen = imagen + "<td>";
                                    imagen = imagen + "<input data-id_archivo='" + value.id_archivo + "' type='file' class='btn-imagen-cargar'>";
                                imagen = imagen + "</td>";
                                // imagen = imagen + "<td>" + value.nombre_distrito + "</td>";
                                //     iniciativas_productivas = iniciativas_productivas + '<a href="#" data-id_caracteristica_especifica_ptfma_promovida="' + value.id_caracteristica_especifica_ptfma_promovida + '" data-id_caracteristica_general="' + value.id_caracteristica_general + '" data-id_id_informacion_general="' + value.id_id_informacion_general + '" data-tipo_formulario="' + value.tipo_formulario + '" class="btn btn-primary btn-tablero" > <i class="fas fa-chalkboard-teacher"></i> </a> ';
                                //     iniciativas_productivas = iniciativas_productivas + '<a href="<?= \Yii::$app->request->BaseUrl ?>/site/imagenes?id_caracteristica_especifica_ptfma_promovida='+ value.id_caracteristica_especifica_ptfma_promovida +'&id_caracteristica_general='+ value.id_caracteristica_general +'&id_id_informacion_general='+ value.id_id_informacion_general +'&tipo_formulario='+ value.tipo_formulario +'" class="btn btn-primary" > <i class="fas fa-chalkboard-teacher"></i> </a> ';
                                // iniciativas_productivas = iniciativas_productivas +"</td>";
                            imagen = imagen + "</tr>";
                        });
                        
                        $('.lista-imagenes tbody').html(imagen);

                        $('.image-popup-vertical-fit').magnificPopup({
                            type: 'image',
                            closeOnContentClick: true,
                            mainClass: 'mfp-img-mobile',
                            image: {
                                verticalFit: true
                            }

                        });
                        $('.lista-imagenes').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "ordering": false,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 10,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){
                            loading.modal('hide');
                        },2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

$('body').on('change', '.btn-imagen-cargar', function (e) {
    var id_archivo = $(this).attr('data-id_archivo');
    var form_data = new FormData();
	form_data.append("_csrf", csrf);
    form_data.append("Foto[archivo]", $(this)[0].files[0]);
    form_data.append("Foto[temp]", "temp");


     $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/site/actualizar-foto?id_archivo='+id_archivo,
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {
                    if(results.success){
                        setTimeout(function(){
                            loading.modal('hide');
                        },2000);
                    }
                    
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
});


</script>